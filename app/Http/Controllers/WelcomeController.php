<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class WelcomeController extends Controller
{

    use AuthenticatesUsers;

    // public function __construct()
    // {
    //     $this->middleware('guest');
    // }

    public function t_login()
    {
        return view('master.welcome');
    }

    public function us_login(Request $request)
    {
        if (Auth::guard('admin')->attempt($request->only('email', 'password'))) {
            return redirect('/dashboard');
        }
        return redirect()->back();
        // dd(auth()->attempt(array('email' => $request['email'])));
    }

    public function us_logout()
    {
        if (auth()->guard('admin')) {
            auth()->guard('admin')->logout();
        }
        session()->flush();
        return redirect('/');
    }
}
