<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class DashboardController extends Controller
{
    use AuthenticatesUsers;
    // public function __construct()
    // {
    //     $this->middleware('guest');
    // }
    public function v_dash()
    {
        return view('master.master');
    }
}
