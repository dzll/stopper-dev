<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
// use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class SuperAdmin extends Authenticatable
{
    use HasFactory, Notifiable;

    protected $table = 'super_admin';
    protected $guard = 'super_admin';
    protected $fillable = [
        'nama', 'email', 'password', 'role'
    ];

    protected $hidden = ['password'];
}
