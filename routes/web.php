<?php

use App\Http\Controllers\DashboardController;
use App\Http\Controllers\WelcomeController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// Auth::routes();
// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/', [WelcomeController::class, 't_login'])->name('rr');
Route::post('/login', [WelcomeController::class, 'us_login'])->name('login');
Route::get('/logout', [WelcomeController::class, 'us_logout'])->name('logout');
#push tes

// Route::any('/{slug}', function () {
//     return view('master.master');
// });
Route::middleware('auth:admin')->group(function () {
    Route::any('/{slug}', [DashboardController::class, 'v_dash']);
});
