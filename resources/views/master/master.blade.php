@extends('layout.app')

@push('css')
{{-- push CSS --}}
@endpush

@section('content')
    {{-- <item-comp></item-comp> --}}
    <router-view></router-view>
@endsection

@push('javascript')
{{-- push JS --}}
@endpush