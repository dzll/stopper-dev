import Vue from 'vue'
import Router from 'vue-router'
Vue.use(Router)

import dashboardPage from './components/pages/Master'
import itemPage from './components/pages/item'
import kartuStok from './components/pages/kartuStok'
import notFound from './components/pages/notfound'

const routes = [
    {
        path: '/dashboard',
        component: dashboardPage
    },
    {
        path: '/daftar-item',
        component: itemPage
    },
    {
        path: '/kartu-stock',
        component: kartuStok
    },
    {
        path: '*',
        component: notFound
    },
]

export default new Router({
    mode: 'history',
    routes
})
