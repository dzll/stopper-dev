/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
//tambahan
require('../assets/js/adminlte.min.js');
require('../assets/js/demo.js');
require('../assets/plugins/overlayScrollbars/js/OverlayScrollbars.min.js');

window.Vue = require('vue').default;
import router from './router'



/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('sidebar-comp', require('./components/Sidebar.vue').default);
Vue.component('header-comp', require('./components/Header.vue').default);
// Vue.component('master', require('./components/pages/Master.vue').default);
// Vue.component('item-comp', require('./components/pages/item.vue').default);
// Vue.component('itemFirst-comp', require('./components/pages/itemFirst.vue').default);


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    router
});
